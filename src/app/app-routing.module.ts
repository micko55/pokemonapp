import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {HomeComponent} from "./_home/home.component";
import {FOfComponent} from "./f-of/f-of.component";

const routes: Routes = [
  {path: "", component:HomeComponent},
  {
    path: "pokemons", //component:PokemonsViewComponent
    loadChildren: () => import('./pokemons/pokemons.module').then(mod => mod.PokemonsModule)
  },
  {
    path: "currency",
    loadChildren: () => import('./currency-exchange/currency.module').then(mod => mod.CurrencyModule)
  },
  {
    path: "banque",
    loadChildren: () => import('./banque/banque.module').then(mod => mod.BanqueModule)
  },
  { path: "**", component: FOfComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
