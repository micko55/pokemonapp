import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {CurrencyAppComponent} from "./currency-view/app.component";
import {FOfComponent} from "../f-of/f-of.component";

const routes: Routes = [
  {path: "", component:CurrencyAppComponent},
  { path: "**", component: FOfComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CurrencyRoutingModule { }
