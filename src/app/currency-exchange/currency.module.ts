import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {CurrencyAppComponent} from "./currency-view/app.component";
import {FormsModule} from "@angular/forms";
import {CurrencyRoutingModule} from "./currency-routing.module";
import {HttpClientModule} from "@angular/common/http";



@NgModule({
  declarations: [
    CurrencyAppComponent,
  ],
  imports: [
    CommonModule,
    CurrencyRoutingModule,
    FormsModule,
    HttpClientModule
  ],
  bootstrap: [CurrencyAppComponent]
})
export class CurrencyModule { }
