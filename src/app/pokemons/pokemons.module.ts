import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {PokemonsViewComponent} from "./pokemons-view/pokemons-view.component";
import {PokemonDetailsComponent} from "./pokemon-details/pokemon-details.component";
import {PokemonTypeColorPipe} from "./pokemon-type-color.pipe";
import {BorderCardDirective} from "./border-card.directive";
import {PokemonsRoutingModule} from "./pokemons-routing.module";
import {PokemonResolver} from "./pokemon-resolver";



@NgModule({
  declarations: [
    PokemonsViewComponent,
    PokemonDetailsComponent,
    PokemonTypeColorPipe,
    BorderCardDirective
  ],
  imports: [
    CommonModule,
    PokemonsRoutingModule
  ],
  providers: [PokemonResolver],
  bootstrap: [PokemonsViewComponent]
})
export class PokemonsModule { }
