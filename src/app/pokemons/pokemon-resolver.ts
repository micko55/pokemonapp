import {ActivatedRouteSnapshot, Resolve, Router} from '@angular/router';
import {EMPTY, Observable, of} from 'rxjs';
import {POKEMONS} from "./mock-pokemons";
import {Injectable} from "@angular/core";

@Injectable()
export class PokemonResolver implements Resolve<any> {
  constructor(private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<any> {
    const pokemonId = Number(route.params["id"]) - 1;
    if (isNaN(pokemonId) || !POKEMONS[pokemonId]) {
      this.router.navigate(["not-found"]).then();
      return EMPTY;
    }
    return of(!EMPTY);
  }
}
