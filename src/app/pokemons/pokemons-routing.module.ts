import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {PokemonDetailsComponent} from "./pokemon-details/pokemon-details.component";
import {PokemonsViewComponent} from "./pokemons-view/pokemons-view.component";
import {FOfComponent} from "../f-of/f-of.component";
import {PokemonResolver} from "./pokemon-resolver";

const routes: Routes = [
  {path: "", component:PokemonsViewComponent},
  {path: ":id", component:PokemonDetailsComponent, resolve: {routeResolver: PokemonResolver}},
  {path: "not-found", component: FOfComponent},
  {path: "**", component: FOfComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PokemonsRoutingModule { }
