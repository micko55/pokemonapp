import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FOfComponent } from './f-of.component';

describe('FOfComponent', () => {
  let component: FOfComponent;
  let fixture: ComponentFixture<FOfComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FOfComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FOfComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
