import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {BanqueAppComponent} from "./banque-view/banque-view.component";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {NewAccountComponent} from "./new-account/new-account.component";
import {TransferComponent} from "./transfer/transfer.component";
import {BanqueRoutingModule} from "./banque-routing.module";



@NgModule({
  declarations: [
    BanqueAppComponent,
    NewAccountComponent,
    TransferComponent
  ],
  imports: [
    CommonModule,
    BanqueRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ],
  bootstrap: [BanqueAppComponent]
})
export class BanqueModule { }
