import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {BanqueAppComponent} from "./banque-view/banque-view.component";
import {NewAccountComponent} from "./new-account/new-account.component";
import {TransferComponent} from "./transfer/transfer.component";
import {FOfComponent} from "../f-of/f-of.component";

const routes: Routes = [
  {path: "", component:BanqueAppComponent},
  {path: "new", component:NewAccountComponent},
  {path: "transfer", component:TransferComponent},
  { path: "**", component: FOfComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BanqueRoutingModule { }
